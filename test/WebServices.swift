//
//  WebServices.swift
//  test
//
//  Created by Havic on 2/6/18.
//  Copyright © 2018 Havic. All rights reserved.
//

import UIKit

protocol JsonDelegate{
    func reloadCollectionData()
}

class WebServices: NSObject {
    var reloadJson: JsonDelegate?
    var pugsArr: [URL]?
    
    public struct Pug: Codable {
        let pugs: [String]
    }
    
    func loadThatJson(from urlString:String) {
        
        fetchPugs(from: urlString) { (pug, error) in
            if let pugs = pug {
                let urlArray: [URL] = pugs.pugs.flatMap {
                    if var url = URLComponents.init(string: $0) {
                        url.host = "media.tumblr.com"
                        return url.url!
                    } else {
                        return nil
                        
                    }
                }
                
                self.pugsArr = urlArray
                
                // Call main thread for UI changes:
                DispatchQueue.main.async {
                    self.reloadJson?.reloadCollectionData()
                }
            }
            else {
                print(error!)
            }
            
        }
       
    }
    
    func fetchPugs(from urlString: String, completion: @escaping (Pug?, Error?) -> ()) {
        if let url = URL.init(string: urlString) {
            var urlRquest = URLRequest.init(url: url)
            urlRquest.httpMethod = "GET"
            
            let sessionTask = URLSession.shared.dataTask(with: urlRquest, completionHandler: { (data, response, error) in
                if let httpResponse = response as? HTTPURLResponse {
                    if 200 ..< 300 ~= httpResponse.statusCode,
                        let respData = data {
                        do {
                            let decoder = JSONDecoder()
                            let pugs: Pug = try decoder.decode(Pug.self, from: respData)
                            
                            completion(pugs, nil)
                            
                        } catch {
                            completion(nil, error)
                        }
                    } else {
                        if error != nil {
                            completion(nil, error)
                        }
                        else {
                            let err = NSError(domain: "pugs", code: 500, userInfo: nil)
                            completion(nil, err)
                        }
                    }
                }
        
            })
            sessionTask.resume()
        }
        else {
            completion(nil, NSError(domain: "pugs", code: 500, userInfo: nil))
        }
    }
}
