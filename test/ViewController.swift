//
//  ViewController.swift
//  test
//
//  Created by Havic on 2/6/18.
//  Copyright © 2018 Havic. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet weak var myCollection: UICollectionView!
    var pugsArr: [URL]?
    
    public struct Pug: Codable {
        let pugs: [String]
    }
    
    
    func reloadCollectionData() {
        print("Collection about to refresh")
        myCollection.reloadData()
        print("Collection has been refreshed")
    }
    
    var jsonUrl = "https://pugme.herokuapp.com/bomb?count=50"
    let webServices = WebServices()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        updateData()
        
    }

    private func updateData() {
        webServices.fetchPugs(from: jsonUrl) { (pug, error) in
            if let pugs = pug {
                let urlArray: [URL] = pugs.pugs.flatMap {
                    if var url = URLComponents.init(string: $0) {
                        url.host = "media.tumblr.com"
                        return url.url!
                    } else {
                        return nil

                    }
                }
                self.pugsArr = urlArray

                // Call main thread for UI changes:
                DispatchQueue.main.async {
                    self.reloadCollectionData()
                }
            }
            else {
                print(error!)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let arrayofPugUrls = pugsArr
        return arrayofPugUrls?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! WDWCollectionViewCell
//        let imageString = "https://vignette.wikia.nocookie.net/cpwuser/images/6/6b/Disney-Black-Logo.jpg"
//        let imageUrl = URL.init(string: imageString)
        if let url = pugsArr?[indexPath.row] {
            do {
                let imageData = try Data.init(contentsOf: url)
                cell.cellImage.image = UIImage(data: imageData)
            } catch {
                print("\(error)")
                let imageString = "https://vignette.wikia.nocookie.net/cpwuser/images/6/6b/Disney-Black-Logo.jpg"
                let imageUrl = URL.init(string: imageString)
                let imageData = NSData.init(contentsOf: imageUrl!)!
                cell.cellImage.image = UIImage(data: imageData as Data)
            }

        }
        
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let pugs = pugsArr else { return }
        if indexPath.row == pugs.count - 1 {  //numberofitem count
            updateData()
        }
    }

    
}
